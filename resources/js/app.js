import 'core-js';
import Vue from 'vue';

require('./bootstrap');

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

if (document.getElementById('app')) {
    new Vue({
        el: '#app',
    });
}
