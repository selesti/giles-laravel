# Giles Assistant

This is a starting point for a CLI helper for Giles.

Currently you can

- Creating a new Laravel project

## Creating a Laravel project

Make sure you have the assistant installed, if you do not, you can install it by running:

```bash
curl -s https://bitbucket.org/selesti/giles-laravel/raw/master/install.sh | bash
```

Once installed you could enter the directory you wish to create your project within e.g.

```bash
mkdir project-2000
cd project-2000
```

Once you're in the project directory !!! THIS IS VERY IMPORTANT !!!

You can then run `giles-assist laravel`

This will create a new project within the **CURRENT** directory.

## Updating

To update the installer just run the install command again as above  ^^

## Contributing

Within this repo are all the default files, the `init.sh` contains all the scripts that run when you start a project so this along with the files can be modified going forward.
