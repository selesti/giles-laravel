#!/usr/bin/env bash
DIR="$(dirname "$(readlink "$0")")"

echo Installing fresh copy of laravel...
composer create-project laravel/laravel ./
php artisan storage:link

echo Initialising git repo...
git init
git add .
git commit -m "Clean laravel install added"

echo Removing presets...
php artisan preset none
git add .

echo Installing PHP helpers...
cp -rf $DIR/app ./

echo Setting up babel...
cp -f $DIR/.babelrc ./

echo Setting up editor config...
cp -f $DIR/.editorconfig ./

echo Setting up javascript linting...
cp -f $DIR/.eslintrc ./

echo Setting up scss linting...
cp -f $DIR/.sasslintrc ./

echo Setting up git ignore...
cp -f $DIR/.gitignore ./

echo Setting up image compression...
cp -f $DIR/.minimage ./
cp -f $DIR/minimage.json ./

echo Setting up git ignore...
cp -f $DIR/.gitignore ./

echo Setting up tailwind css...
cp -f $DIR/tailwind.js ./
cp -rf $DIR/resources ./

echo Setting up webpack...
cp -f $DIR/webpack.mix.js ./

echo Setting up humans.txt...
cp -rf $DIR/public ./

echo Setting up git hooks...
cp -rf $DIR/git-hooks ./
ln -s ./git-hooks .git/hooks
chmod +x ./git-hooks/pre-push

echo Setting up vhost entry...
ln -s public httpdocs

echo Setting up composer packages...
composer require predis/predis laravel/horizon owenmelbz/domain-enforcement owenmelbz/laravel-robots-txt sentry/sentry-laravel doctrine/dbal genealabs/laravel-model-caching guzzlehttp/guzzle owenmelbz/vue-impersonate paragonie/csp-builder tightenco/ziggy
composer require squizlabs/php_codesniffer barryvdh/laravel-debugbar barryvdh/laravel-ide-helper --dev
php artisan ide-helper:generate
php artisan ide-helper:meta

echo Setting up php code sniffer...
cp -f $DIR/phpcs.xml ./
./vendor/bin/phpcbf --standard=phpcs.xml --warning-severity=8 ./app

echo Setting up npm...
cp $DIR/.npmrc ./
rm package-lock.json || true > /dev/null

echo Installing npm dependencies
cp $DIR/package.json ./

npm whoami > /dev/null

if [[ $? -eq 0 ]]; then
    echo Already logged into npm...
else
    npm login
fi

npm install
git add .
git commit -m "Added Giles files"

echo Building initial assets
npm run dev

if [[ -x ssler ]]; then
    echo Installing SSL...
    ssler `basename \"$PWD\"`.dev
fi
