#!/usr/bin/env bash

# curl -s https://bitbucket.org/selesti/giles-laravel/raw/master/install.sh | bash

if [[ -d ~/.giles/laravel ]]; then
  cd ~/.giles/laravel
  git pull
  echo Giles Assistant updated...
  exit;
fi

echo Installing Giles Assistant...
mkdir -p ~/.giles
git clone git@bitbucket.org:selesti/giles-laravel.git ~/.giles/laravel
ln -s ~/.giles/laravel/init.sh /usr/local/bin/giles-assist
